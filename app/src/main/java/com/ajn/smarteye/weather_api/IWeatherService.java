package com.ajn.smarteye.weather_api;

import com.ajn.smarteye.weather_api.models.CurrentWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IWeatherService {

    @GET("current") Call<CurrentWeather> getCurrentWeather(@Query("lat") Double lat, @Query("lon") Double lon, @Query("key") String api_key);
}
