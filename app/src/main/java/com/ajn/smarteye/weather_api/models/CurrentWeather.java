package com.ajn.smarteye.weather_api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrentWeather {
    @SerializedName("data") public List<Weather> weatherList;

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public void setWeatherList(List<Weather> weatherList) {
        this.weatherList = weatherList;
    }

    @Override
    public String toString() {
        return "CurrentWeather{" +
                "weatherList=" + weatherList +
                '}';
    }
}
