package com.ajn.smarteye.weather_api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Weather {
    @SerializedName("wind_cdir") private String wind_cdir;
    @SerializedName("rh") private double rh;
    @SerializedName("pod") private String pod;
    @SerializedName("lon") private double lon;
    @SerializedName("pres") private Double pres;
    @SerializedName("timezone") private String timezone;
    @SerializedName("ob_time") private String ob_time;
    @SerializedName("country_code") private String country_code;
    @SerializedName("clouds") private double clouds;
    @SerializedName("vis") private Double vis;
    @SerializedName("state_code") private int state_code;
    @SerializedName("wind_spd") private Double wind_spd;
    @SerializedName("lat") private Double lat;
    @SerializedName("wind_cdir_full") private String wind_cdir_full;
    @SerializedName("slp") private Double slp;
    @SerializedName("datetime") private String datetime;
    @SerializedName("ts") private Double ts;
    @SerializedName("station") private String station;
    @SerializedName("h_angle") private double h_angle;
    @SerializedName("dewpt") private Double dewpt;
    @SerializedName("uv") private double uv;
    @SerializedName("dni") private double dni;
    @SerializedName("precip") private double precip;
    @SerializedName("city_name") private String city_name;
    @SerializedName("sunset") private String sunset;
    @SerializedName("temp") private Double temp;
    @SerializedName("sunrise") private String sunrise;
    @SerializedName("app_temp") private Double app_temp;
    @SerializedName("weather") private WeatherDetail weather;

    public WeatherDetail getWeather() {
        return weather;
    }

    public void setWeather(WeatherDetail weather) {
        this.weather = weather;
    }

    public String getWind_cdir() {
        return wind_cdir;
    }

    public void setWind_cdir(String wind_cdir) {
        this.wind_cdir = wind_cdir;
    }

    public double getRh() {
        return rh;
    }

    public void setRh(int rh) {
        this.rh = rh;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(int lon) {
        this.lon = lon;
    }

    public Double getPres() {
        return pres;
    }

    public void setPres(Double pres) {
        this.pres = pres;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getOb_time() {
        return ob_time;
    }

    public void setOb_time(String ob_time) {
        this.ob_time = ob_time;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public double getClouds() {
        return clouds;
    }

    public void setClouds(int clouds) {
        this.clouds = clouds;
    }

    public Double getVis() {
        return vis;
    }

    public void setVis(Double vis) {
        this.vis = vis;
    }

    public int getState_code() {
        return state_code;
    }

    public void setState_code(int state_code) {
        this.state_code = state_code;
    }

    public Double getWind_spd() {
        return wind_spd;
    }

    public void setWind_spd(Double wind_spd) {
        this.wind_spd = wind_spd;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getWind_cdir_full() {
        return wind_cdir_full;
    }

    public void setWind_cdir_full(String wind_cdir_full) {
        this.wind_cdir_full = wind_cdir_full;
    }

    public Double getSlp() {
        return slp;
    }

    public void setSlp(Double slp) {
        this.slp = slp;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Double getTs() {
        return ts;
    }

    public void setTs(Double ts) {
        this.ts = ts;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public double getH_angle() {
        return h_angle;
    }

    public void setH_angle(int h_angle) {
        this.h_angle = h_angle;
    }

    public Double getDewpt() {
        return dewpt;
    }

    public void setDewpt(Double dewpt) {
        this.dewpt = dewpt;
    }

    public double getUv() {
        return uv;
    }

    public void setUv(int uv) {
        this.uv = uv;
    }

    public double getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public double getPrecip() {
        return precip;
    }

    public void setPrecip(int precip) {
        this.precip = precip;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public Double getApp_temp() {
        return app_temp;
    }

    public void setApp_temp(Double app_temp) {
        this.app_temp = app_temp;
    }

    private Weather(Builder builder) {
        wind_cdir = builder.wind_cdir;
        rh = builder.rh;
        pod = builder.pod;
        lon = builder.lon;
        pres = builder.pres;
        timezone = builder.timezone;
        ob_time = builder.ob_time;
        country_code = builder.country_code;
        clouds = builder.clouds;
        vis = builder.vis;
        state_code = builder.state_code;
        wind_spd = builder.wind_spd;
        lat = builder.lat;
        wind_cdir_full = builder.wind_cdir_full;
        slp = builder.slp;
        datetime = builder.datetime;
        ts = builder.ts;
        station = builder.station;
        h_angle = builder.h_angle;
        dewpt = builder.dewpt;
        uv = builder.uv;
        dni = builder.dni;
        precip = builder.precip;
        city_name = builder.city_name;
        sunset = builder.sunset;
        temp = builder.temp;
        sunrise = builder.sunrise;
        app_temp = builder.app_temp;
    }


    public static final class Builder {
        private String wind_cdir;
        private int rh;
        private String pod;
        private int lon;
        private Double pres;
        private String timezone;
        private String ob_time;
        private String country_code;
        private int clouds;
        private Double vis;
        private int state_code;
        private Double wind_spd;
        private Double lat;
        private String wind_cdir_full;
        private Double slp;
        private String datetime;
        private Double ts;
        private String station;
        private int h_angle;
        private Double dewpt;
        private int uv;
        private int dni;
        private int precip;
        private String city_name;
        private String sunset;
        private Double temp;
        private String sunrise;
        private Double app_temp;

        public Builder() {
        }

        public Builder wind_cdir(String val) {
            wind_cdir = val;
            return this;
        }

        public Builder rh(int val) {
            rh = val;
            return this;
        }

        public Builder pod(String val) {
            pod = val;
            return this;
        }

        public Builder lon(int val) {
            lon = val;
            return this;
        }

        public Builder pres(Double val) {
            pres = val;
            return this;
        }

        public Builder timezone(String val) {
            timezone = val;
            return this;
        }

        public Builder ob_time(String val) {
            ob_time = val;
            return this;
        }

        public Builder country_code(String val) {
            country_code = val;
            return this;
        }

        public Builder clouds(int val) {
            clouds = val;
            return this;
        }

        public Builder vis(Double val) {
            vis = val;
            return this;
        }

        public Builder state_code(int val) {
            state_code = val;
            return this;
        }

        public Builder wind_spd(Double val) {
            wind_spd = val;
            return this;
        }

        public Builder lat(Double val) {
            lat = val;
            return this;
        }

        public Builder wind_cdir_full(String val) {
            wind_cdir_full = val;
            return this;
        }

        public Builder slp(Double val) {
            slp = val;
            return this;
        }

        public Builder datetime(String val) {
            datetime = val;
            return this;
        }

        public Builder ts(Double val) {
            ts = val;
            return this;
        }

        public Builder station(String val) {
            station = val;
            return this;
        }

        public Builder h_angle(int val) {
            h_angle = val;
            return this;
        }

        public Builder dewpt(Double val) {
            dewpt = val;
            return this;
        }

        public Builder uv(int val) {
            uv = val;
            return this;
        }

        public Builder dni(int val) {
            dni = val;
            return this;
        }

        public Builder precip(int val) {
            precip = val;
            return this;
        }

        public Builder city_name(String val) {
            city_name = val;
            return this;
        }

        public Builder sunset(String val) {
            sunset = val;
            return this;
        }

        public Builder temp(Double val) {
            temp = val;
            return this;
        }

        public Builder sunrise(String val) {
            sunrise = val;
            return this;
        }

        public Builder app_temp(Double val) {
            app_temp = val;
            return this;
        }

        public Weather build() {
            return new Weather(this);
        }
    }

    @Override
    public String toString() {
        return "Weather{" +
                "wind_cdir='" + wind_cdir + '\'' +
                ", rh=" + rh +
                ", pod='" + pod + '\'' +
                ", lon=" + lon +
                ", pres=" + pres +
                ", timezone='" + timezone + '\'' +
                ", ob_time='" + ob_time + '\'' +
                ", country_code='" + country_code + '\'' +
                ", clouds=" + clouds +
                ", vis=" + vis +
                ", state_code=" + state_code +
                ", wind_spd=" + wind_spd +
                ", lat=" + lat +
                ", wind_cdir_full='" + wind_cdir_full + '\'' +
                ", slp=" + slp +
                ", datetime='" + datetime + '\'' +
                ", ts=" + ts +
                ", station='" + station + '\'' +
                ", h_angle=" + h_angle +
                ", dewpt=" + dewpt +
                ", uv=" + uv +
                ", dni=" + dni +
                ", precip=" + precip +
                ", city_name='" + city_name + '\'' +
                ", sunset='" + sunset + '\'' +
                ", temp=" + temp +
                ", sunrise='" + sunrise + '\'' +
                ", app_temp=" + app_temp +
                '}';
    }
}
