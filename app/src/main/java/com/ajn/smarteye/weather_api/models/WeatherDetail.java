package com.ajn.smarteye.weather_api.models;

import com.google.gson.annotations.SerializedName;

public class WeatherDetail {

    @SerializedName("icon") private String icon;
    @SerializedName("code") private String code;
    @SerializedName("description") private String description;

    /**
     * No args constructor for use in serialization
     */
    public WeatherDetail() {
    }

    /**
     * @param icon
     * @param description
     * @param code
     */
    public WeatherDetail(String icon, String code, String description) {
        super();
        this.icon = icon;
        this.code = code;
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
