package com.ajn.smarteye.weather_api;

import android.util.Log;

import com.ajn.smarteye.weather_api.models.CurrentWeather;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherProvider {
    private static WeatherProvider INSTANCE = null;
    private IWeatherService iWeatherService;

    private WeatherProvider() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.weatherbit.io/v2.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(createOkHttpClient())
                .build();
        this.iWeatherService = retrofit.create(IWeatherService.class);
    }

    public static synchronized WeatherProvider getINSTANCE() {
        if (INSTANCE == null)
            INSTANCE = new WeatherProvider();
        return INSTANCE;
    }

    private OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        return builder.build();
    }

    public void getCurentWeather(Double lat, Double lon, final IWeatherListner<CurrentWeather> listner) {
        iWeatherService.getCurrentWeather(lat,lon,"656e887dc8f34a79a3c3f134ba907b92").enqueue(new Callback<CurrentWeather>() {
            @Override
            public void onResponse(Call<CurrentWeather> call, Response<CurrentWeather> response) {
                if (response.code() != 200) {
                    Log.e("(GET) CURRENT_WEATHER",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                    listner.onError("ERROR GET ALL UP COMMING MOVIES LIST");
                    return;
                }
                CurrentWeather currentWeather = response.body();
                Log.i("(GET) CURRENT_WEATHER",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                listner.onSuccess(currentWeather);
            }

            @Override
            public void onFailure(Call<CurrentWeather> call, Throwable t) {
                Log.e("(GET) CURRENT_WEATHER", "ERROR WEB SERVICES\n\n" + t.getMessage());
                listner.onError(t.getMessage());
            }
        });
    }
}
