package com.ajn.smarteye.weather_api;

public interface IWeatherListner<T> {
    void onSuccess(T response);
    void onError(String s);
}
