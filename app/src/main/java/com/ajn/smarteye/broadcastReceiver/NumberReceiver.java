package com.ajn.smarteye.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import com.ajn.smarteye.helpers.BLEUartHelper;
import com.ajn.smarteye.pojo.Notification;

import java.util.Date;
import static com.ajn.smarteye.helpers.BroadcastHelper.getContactName;

/**
 * Created by apple on 16/05/2018.
 */

public class NumberReceiver extends BroadcastReceiver {

    private static String savedNumber;
    BLEUartHelper bleUartHelper = BLEUartHelper.getInstance();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
        } else{
            //https://developer.android.com/reference/android/telephony/TelephonyManager tous les états
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            String contact = getContactName(context, number);

            if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE) ||
                stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING) ){
                bleUartHelper.sendMessage(new Notification("notification","call",number,contact).toString());
            }
        }
    }
}