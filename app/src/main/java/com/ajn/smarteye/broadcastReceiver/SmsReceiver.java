package com.ajn.smarteye.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.ajn.smarteye.helpers.BLEUartHelper;
import com.ajn.smarteye.pojo.Notification;

import static com.ajn.smarteye.helpers.BroadcastHelper.getContactName;

/**
 * Created by apple on 20/05/2018.
 */

public class SmsReceiver extends BroadcastReceiver {

    BLEUartHelper bleUartHelper = BLEUartHelper.getInstance();

    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String contact = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();
                    bleUartHelper.sendMessage(new Notification("notification","message",contact,message).toString());
                }
            }
        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);
        }
    }
}
