package com.ajn.smarteye.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.IBinder;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ajn.smarteye.helpers.ActionMessage;
import com.ajn.smarteye.helpers.BLEUartHelper;
import com.ajn.smarteye.helpers.WeatherMessage;
import com.ajn.smarteye.weather_api.IWeatherListner;
import com.ajn.smarteye.weather_api.WeatherProvider;
import com.ajn.smarteye.weather_api.models.CurrentWeather;
import com.ajn.smarteye.weather_api.models.Weather;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.ajn.smarteye.services.DeviceService.getCalendarEvent;

/**
 * Created by apple on 22/05/2018.
 */

public class DataProviderService extends Service {

    private BLEUartHelper bleUartHelper = BLEUartHelper.getInstance();

    private static final int HIGH_FREQUENCY_INTERVAL = 6000;
    private static final int LOW_FREQUENCY_INTERVAL = 600000;
    private static final FitnessOptions fitnessOptions =
            FitnessOptions.builder()
                    .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                    .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                    .build();

    private static String calendarCache = "";

    @Override
    public void onCreate() {
        super.onCreate();
        // High frequency update
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Calendar
                String calendarDataJson = getCalendarEvent(getBaseContext());
                // Do not send if celendar data was not updated
                if(calendarCache.compareToIgnoreCase(calendarDataJson) != 0) {
                    calendarCache = calendarDataJson;
                    if (calendarDataJson != null) {
                        bleUartHelper.sendMessage(calendarDataJson);
                    }
                }
                // Steps
                if (GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(getBaseContext()), fitnessOptions)) {
                    readData(getBaseContext());
                }
            }
        }, 0, HIGH_FREQUENCY_INTERVAL);

        // Low frequency update
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Weather
                getWeatherDateAndSend();
            }
        }, 0, LOW_FREQUENCY_INTERVAL);

    }

    private void getWeatherDateAndSend() {
        Double lat = 48.866667;
        Double lon = 2.333333;

        WeatherProvider.getINSTANCE().getCurentWeather(lat, lon, new IWeatherListner<CurrentWeather>() {
            @Override
            public void onSuccess(CurrentWeather response) {
                Weather currentWeather = response.getWeatherList().get(0);

                WeatherMessage message = new WeatherMessage();
                message.setMessageType("weather");
                message.setTarget("weather");
                message.setValue(currentWeather);

                Gson gson = new Gson();
                bleUartHelper.sendMessage(gson.toJson(message));

                Log.d("RESULT", response.toString());
            }

            @Override
            public void onError(String s) {
                Log.d("RESULT", "ERROR");
            }
        });
    }


    private void readData(Context context) {
        Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(context))
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener(
                        new OnSuccessListener<DataSet>() {
                            @Override
                            public void onSuccess(DataSet dataSet) {
                                long total =
                                        dataSet.isEmpty()
                                                ? 0
                                                : dataSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();
                                bleUartHelper.sendMessage(new ActionMessage("steps",total).toString());
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("", "Error step count", e);
                            }
                        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
