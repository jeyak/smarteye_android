package com.ajn.smarteye.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.ajn.smarteye.MainActivity;
import com.ajn.smarteye.helpers.WeatherMessage;
import com.ajn.smarteye.pojo.CalendarEvent;
import com.ajn.smarteye.pojo.CalendarEventMessage;
import com.ajn.smarteye.weather_api.IWeatherListner;
import com.ajn.smarteye.weather_api.WeatherProvider;
import com.ajn.smarteye.weather_api.models.CurrentWeather;
import com.ajn.smarteye.weather_api.models.Weather;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by apple on 14/06/2018.
 */

public abstract class DeviceService {

    @SuppressLint("MissingPermission")
    public static String getCalendarEvent(Context context) {
        Cursor cur;
        String[] mProjection =
                {
                        "_id",
                        CalendarContract.Events.TITLE,
                        CalendarContract.Events.EVENT_LOCATION,
                        CalendarContract.Events.DTSTART,
                        CalendarContract.Events.DTEND,
                };
        Uri uri = CalendarContract.Events.CONTENT_URI;
        String selection = CalendarContract.Events.DTSTART + " > " + Calendar.getInstance().getTime().getTime();

        ContentResolver cr = context.getContentResolver();
        cur = cr.query(uri, mProjection, selection, null, CalendarContract.Events.DTSTART + " ASC");
        CalendarEvent[] events = new CalendarEvent[2];

        for(int i = 0 ; i <= 1; i++) {
            cur.moveToNext();
            String title = cur.getString(cur.getColumnIndex(CalendarContract.Events.TITLE));
            String eventLocation = cur.getString(cur.getColumnIndex(CalendarContract.Events.EVENT_LOCATION));
            if(eventLocation.length() > 16) {
                eventLocation = eventLocation.substring(0, 16) + " ...";
            }
            String start = formatDate(cur.getString(cur.getColumnIndex(CalendarContract.Events.DTSTART)));
            String end = formatDate(cur.getString(cur.getColumnIndex(CalendarContract.Events.DTEND)));
            events[i] = new CalendarEvent(title, eventLocation, start, end);
        }
        CalendarEventMessage calendarEvent = new CalendarEventMessage("calendar", "calendar_event", events);
        return calendarEvent.toString();
    }


    private static String formatDate(String string) {
        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/YY");
        return dateFormat.format(new Date(Long.valueOf(string)));
    }
}
