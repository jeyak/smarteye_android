package com.ajn.smarteye.settings;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ajn.smarteye.R;
import com.ajn.smarteye.dragNdropHelper.IItemTouchHelperAdapter;
import com.ajn.smarteye.dragNdropHelper.IItemTouchHelperViewHolder;
import com.ajn.smarteye.dragNdropHelper.IOnCustomerListChangedListener;
import com.ajn.smarteye.dragNdropHelper.IOnStartDragListener;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SEWidgetAdapter extends RecyclerView.Adapter<SEWidgetAdapter.SEWidgetViewHolder> implements IItemTouchHelperAdapter {

    private List<SEWidget> seWidgetList;
    private IOnStartDragListener dragListener;
    private IOnCustomerListChangedListener listChangedListener;

    public SEWidgetAdapter(List<SEWidget> seWidgetList, IOnStartDragListener dragListener, IOnCustomerListChangedListener listChangedListener) {
        this.dragListener = dragListener;
        this.listChangedListener = listChangedListener;
        this.seWidgetList = seWidgetList;
    }

    @NonNull
    @Override
    public SEWidgetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View widgetItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_settings_widget, parent, false);
        return new SEWidgetViewHolder(widgetItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final SEWidgetViewHolder holder, int position) {
        final SEWidget selectedWidget = seWidgetList.get(position);
        holder.title.setText(selectedWidget.getTitle());

        holder.imgWidget.setImageDrawable(selectedWidget.getImg());


        holder.handle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    dragListener.onStartDrag(holder);
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return seWidgetList.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(seWidgetList, fromPosition, toPosition);
        listChangedListener.onNoteListChanged(seWidgetList);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {

    }

    class SEWidgetViewHolder extends RecyclerView.ViewHolder implements IItemTouchHelperViewHolder {

        @BindView(R.id.title_widget)
        TextView title;
        @BindView(R.id.handle)
        ImageView handle;
        @BindView(R.id.img_widget)
        ImageView imgWidget;

        public SEWidgetViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onItemSelected() {
            Log.d("AZERTYUIOP", "onItemSelected()");
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}
