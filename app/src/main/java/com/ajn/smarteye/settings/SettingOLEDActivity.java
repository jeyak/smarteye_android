package com.ajn.smarteye.settings;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ajn.smarteye.R;
import com.ajn.smarteye.dragNdropHelper.IOnCustomerListChangedListener;
import com.ajn.smarteye.dragNdropHelper.IOnStartDragListener;
import com.ajn.smarteye.dragNdropHelper.SimpleItemTouchHelperCallback;
import com.ajn.smarteye.helpers.BLEUartHelper;
import com.ajn.smarteye.helpers.IBLEUartHelperEventHandler;
import com.ajn.smarteye.helpers.ActionMessage;
import com.ajn.smarteye.helpers.MiscMessage;
import com.ajn.smarteye.helpers.RequestMessage;
import com.ajn.smarteye.helpers.ViewOrderMessage;
import com.ajn.smarteye.helpers.ViewOrderObject;
import com.ajn.smarteye.helpers.WeatherMessage;
import com.ajn.smarteye.weather_api.IWeatherListner;
import com.ajn.smarteye.weather_api.WeatherProvider;
import com.ajn.smarteye.weather_api.models.CurrentWeather;
import com.ajn.smarteye.weather_api.models.Weather;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingOLEDActivity extends AppCompatActivity implements IOnCustomerListChangedListener, IOnStartDragListener, IBLEUartHelperEventHandler {

    // BLE STATE
    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_READY = 10;
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;

    private Weather currentWeather = null;

    private int mState = UART_PROFILE_DISCONNECTED;

    @BindView(R.id.power_on)
    Button powerOn;
    @BindView(R.id.power_off)
    Button powerOff;
    @BindView(R.id.motion_sensor_on)
    Button sensorOn;
    @BindView(R.id.motion_sensor_off)
    Button sensorOff;
    @BindView(R.id.settings_title)
    TextView title;
    @BindView(R.id.seekBar)
    SeekBar brightness;
    @BindView(R.id.recycler_settings)
    RecyclerView recyclerView;

    private List<SEWidget> seWidgetList;
    private ItemTouchHelper itemTouchHelper;

    BLEUartHelper bleUartHelper = BLEUartHelper.getInstance();

    private String TAG = "Settings";

    public static String TITLE_DEVICE_PARAM = "TITLE_DEVICE_PARAM";

    private final String LIST_OF_SORTED_DATA_ID = "json_list_sorted_data_id";
    private final String POWER_DATA_ID = "power_data_id";
    private final String SENSOR_DATA_ID = "sensor_data_id";
    private final String BRIGHTNESS_DATA_ID = "brightness_data_id";
    private final String SHARED_PREF = "DEVICE_SETTINGS";

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            final MiscMessage message = new MiscMessage("battery_level", String.valueOf(level));
            final Gson gson = new Gson();
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    bleUartHelper.sendMessage(gson.toJson(message));
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setting_oled);
        ButterKnife.bind(this);
        title.setText(getIntent().getStringExtra(TITLE_DEVICE_PARAM));
        seWidgetList = initData();
        initializeSeekBarBrightness();
        setupRecyclerView();

        bleUartHelper.addEventHandler(this);

        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    @Override
    protected void onResume() {
        super.onResume();

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                getWeatherAndSend();
                getDeviceNameAndSend();
                getOperatorNameAndSend();
            }
        });
    }

    private List<SEWidget> initData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        initBtnPower(sharedPreferences.getInt(POWER_DATA_ID, 1));
        initBtnSensor(sharedPreferences.getInt(SENSOR_DATA_ID, 1));
        initSeekBar(sharedPreferences.getInt(BRIGHTNESS_DATA_ID, 100));
        return initseWidgetList(sharedPreferences.getString(LIST_OF_SORTED_DATA_ID, ""));
    }

    private void initBtnPower(int data) {
        messageBle("screen", data);
        if (data == 1) {
            powerOnClick();
            return;
        }
        powerOffClick();
    }

    private void initBtnSensor(int data) {
        messageBle("motion_sensor", data);
        if (data == 1) {
            sensorOnClick();
            return;
        }
        sensorOffClick();
    }

    private void initSeekBar(int data) {
        brightness.setProgress(data);
        messageBle("brightness", data);
    }

    private List<SEWidget> baseList() {
        String[] strings = {"Clock", "Weather", "Calendar", "Step counter"};
        int[] ints = {R.drawable.ic_time, R.drawable.weather, R.drawable.calendar, R.drawable.ic_map };
        List<SEWidget> widgetList = new ArrayList<>();
        for (int i = 0; i < strings.length; i++) {
            widgetList.add(new SEWidget(i + 1, strings[i], getDrawable(ints[i])));
        }
        return widgetList;
    }

    private List<SEWidget> initseWidgetList(String jsonListOfSortedCustomerId) {
        List<SEWidget> seWidgetListSorted = new ArrayList<>();

        List<SEWidget> widgetList = baseList();

        if (jsonListOfSortedCustomerId.isEmpty()) {
            return widgetList;
        }

        Gson gson = new Gson();

        List<Integer> listOfSortedWidgetsId = gson.fromJson(jsonListOfSortedCustomerId, new TypeToken<List<Integer>>(){}.getType());

        if (listOfSortedWidgetsId != null && listOfSortedWidgetsId.size() > 0) {
            for (Integer id: listOfSortedWidgetsId) {
                for (SEWidget widget: widgetList) {
                    if (widget.getId() == id) {
                        seWidgetListSorted.add(widget);
                        widgetList.remove(widget);
                        break;
                    }
                }
            }
        }

        if (widgetList.size() > 0) {
            seWidgetListSorted.addAll(widgetList);
        }

        sendViewOrder(listOfSortedWidgetsId);

        return seWidgetListSorted;
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        SEWidgetAdapter adapter = new SEWidgetAdapter(seWidgetList, this, this);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);
    }

    private void initializeSeekBarBrightness() {
        brightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                //Log.d("MY_DEBUG", "Seek Boolean= " + b + ", Seek value= " + i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int value = seekBar.getProgress();
                if (value < 30) {
                    value = 30;
                    seekBar.setProgress(value);
                }
                Log.d("MY_DEBUG", "Seek Stop value= " + value);
                btnSavePref(BRIGHTNESS_DATA_ID, value);
                messageBle("brightness", value);
            }
        });
    }

    @Override
    public void onNoteListChanged(List<SEWidget> seWidgetList) {
        Gson gson = new Gson();
        List<Integer> listOfSortedWidgetId = new ArrayList<>();
        for (SEWidget widget: seWidgetList)
            listOfSortedWidgetId.add(widget.getId());
        savePref(gson.toJson(listOfSortedWidgetId));


        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        String data = sharedPreferences.getString(LIST_OF_SORTED_DATA_ID, "");
        List<Integer> listOfSortedWidgetsId = gson.fromJson(data, new TypeToken<List<Integer>>(){}.getType());




        sendViewOrder(listOfSortedWidgetsId);

    }

    private void sendViewOrder(List<Integer> listOfSortedWidgetId) {
        final Gson gson = new Gson();

        ViewOrderObject viewOrderObject = new ViewOrderObject();

        viewOrderObject.setClock(listOfSortedWidgetId.indexOf(1) + 1);
        viewOrderObject.setWeather(listOfSortedWidgetId.indexOf(2) + 1);
        viewOrderObject.setCalendar(listOfSortedWidgetId.indexOf(3) + 1);
        viewOrderObject.setPodometer(listOfSortedWidgetId.indexOf(4) + 1);

        final ViewOrderMessage messageObject = new ViewOrderMessage();

        messageObject.setMessageType("view_order_update");
        messageObject.setTarget("view_order");
        messageObject.setValue(viewOrderObject);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bleUartHelper.sendMessage(gson.toJson(messageObject));
            }
        });

    }

    private void savePref(String jsonListOfSortedCustomerIds) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LIST_OF_SORTED_DATA_ID, jsonListOfSortedCustomerIds).apply();
        editor.apply();
    }

    private void btnSavePref(String dataID, int value) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(dataID, value).apply();
        editor.apply();
    }

    public void messageBle(String target, int value) {
        final ActionMessage message = new ActionMessage(target, value);
        final Gson gson = new Gson();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bleUartHelper.sendMessage(gson.toJson(message));
            }
        });
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
    }

    @OnClick(R.id.btn_connect) public void disconnectClick() {
        Log.d("SETTINGS", "Click on disconnect");
        bleUartHelper.disconnect();
        finish();
    }

    @OnClick(R.id.power_on) public void powerOnClick() {
        powerOn.setBackground(getDrawable(R.drawable.selectbutton));
        powerOff.setBackground(getDrawable(R.drawable.roundedbutton));
        btnSavePref(POWER_DATA_ID, 1);
        messageBle("screen", 1);
    }
    @OnClick(R.id.power_off) public void powerOffClick() {
        powerOff.setBackground(getDrawable(R.drawable.selectbutton));
        powerOn.setBackground(getDrawable(R.drawable.roundedbutton));
        btnSavePref(POWER_DATA_ID, 0);
        messageBle("screen", 0);
    }
    @OnClick(R.id.motion_sensor_on) public void sensorOnClick() {
        sensorOn.setBackground(getDrawable(R.drawable.selectbutton));
        sensorOff.setBackground(getDrawable(R.drawable.roundedbutton));
        btnSavePref(SENSOR_DATA_ID, 1);
        messageBle("motion_sensor", 1);
    }
    @OnClick(R.id.motion_sensor_off) public void sensorOffClick() {
        sensorOff.setBackground(getDrawable(R.drawable.selectbutton));
        sensorOn.setBackground(getDrawable(R.drawable.roundedbutton));
        btnSavePref(SENSOR_DATA_ID, 0);
        messageBle("motion_sensor", 0);
    }

    public void getWeatherAndSend() {
        // Paris coord
        Double lat = 48.866667;
        Double lon = 2.333333;

        WeatherProvider.getINSTANCE().getCurentWeather(lat, lon, new IWeatherListner<CurrentWeather>() {
            @Override
            public void onSuccess(CurrentWeather response) {
                currentWeather = response.getWeatherList().get(0);
                sendWeatherToSmartEye(currentWeather);
                Log.d("RESULT", response.toString());
            }

            @Override
            public void onError(String s) {
                Log.d("RESULT", "ERROR");
            }
        });
    }

    public void sendWeatherToSmartEye(Weather weather) {
        final WeatherMessage message = new WeatherMessage();
        message.setMessageType("weather");
        message.setTarget("weather");
        message.setValue(currentWeather);

        final Gson gson = new Gson();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bleUartHelper.sendMessage(gson.toJson(message));
            }
        });
    }

    private void getDeviceNameAndSend() {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        String deviceName = myDevice.getName();

        final MiscMessage message = new MiscMessage("device_name", deviceName);
        final Gson gson = new Gson();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bleUartHelper.sendMessage(gson.toJson(message));
            }
        });
    }

    private void getOperatorNameAndSend() {
        TelephonyManager manager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        final MiscMessage message = new MiscMessage("operator_name", carrierName);
        final Gson gson = new Gson();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bleUartHelper.sendMessage(gson.toJson(message));
            }
        });
    }

    @Override
    public void onMessageReceived(String message) {
        Gson gson = new Gson();
        RequestMessage requestMessage = gson.fromJson(message, RequestMessage.class);

        if(requestMessage != null) {
            switch (requestMessage.getTarget()) {
                case "weather":
                    getWeatherAndSend();
                    break;
                case "calendar_event":

                    break;
            }
        }
    }

    @Override
    public void onDeviceConnected() {

    }

    @Override
    public void onDeviceReady() {

    }

    @Override
    public void onDeviceDisconnected() {
        finish();
    }

    @Override
    public void onBluetoothInitializationFailed() {

    }

    @Override
    public void onDeviceConnectionFailed() {

    }
}
