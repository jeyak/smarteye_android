package com.ajn.smarteye.settings;

import android.graphics.drawable.Drawable;

public class SEWidget {

    private int id;
    private String title;
    private Drawable img;

    public SEWidget(int id, String title, Drawable img) {
        this.id = id;
        this.title = title;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getImg() {
        return img;
    }
}
