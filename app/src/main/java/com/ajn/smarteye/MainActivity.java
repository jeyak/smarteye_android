package com.ajn.smarteye;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import com.ajn.smarteye.broadcastReceiver.NumberReceiver;
import com.ajn.smarteye.broadcastReceiver.SmsReceiver;
import com.ajn.smarteye.fit.LogWrapper;
import com.ajn.smarteye.fit.MessageOnlyLogFilter;
import com.ajn.smarteye.helpers.BLEUartHelper;
import com.ajn.smarteye.helpers.IBLEUartHelperEventHandler;
import com.ajn.smarteye.helpers.SVGLogoLoader;
import com.ajn.smarteye.services.DataProviderService;
import com.ajn.smarteye.settings.SettingOLEDActivity;
import com.ajn.smarteye.weather_api.IWeatherListner;
import com.ajn.smarteye.weather_api.WeatherProvider;
import com.ajn.smarteye.weather_api.models.CurrentWeather;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.jaredrummler.android.widget.AnimatedSvgView;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity implements IBLEUartHelperEventHandler {

    private static final String TAG = "MainActivity";

    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";

    private android.app.AlertDialog enableNotificationListenerAlertDialog;

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;


    private BLEUartHelper bleUartHelper = BLEUartHelper.getInstance();

    private boolean mScanning;
    private Handler mHandler = new Handler();

    private BleAdapter listBleAdapter;

    private static final int REQUEST_OAUTH_REQUEST_CODE = 0x1001;


    private int numSteps;

    private final int timeout = 2;
    private int cmp = 0;


    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    @BindView(R.id.recycler_view_ble) RecyclerView bleRecyclerView;
    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipeRefreshLayout;
    //connectingMessageLayout
    @BindView(R.id.connectingMessageLayout) ConstraintLayout connectingMessageLayout;
    @BindView(R.id.connectingMessageTextView) TextView connectingMessageTextView;

    @BindView(R.id.animated_svg_view) AnimatedSvgView svgView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        //PERMISSION
        String[] permissions = {Manifest.permission.READ_CONTACTS,Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS,Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_CALENDAR,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET};
        ActivityCompat.requestPermissions(this, permissions, 1);

        if(!isNotificationServiceEnabled()){
            enableNotificationListenerAlertDialog = buildNotificationServiceAlertDialog();
            enableNotificationListenerAlertDialog.show();
        }

        uartServiceInit();

        initializeLogging();

        FitnessOptions fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                        .build();
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this,
                    REQUEST_OAUTH_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions);
        } else {
            subscribe();
        }

        InitializePullToRefresh();
        bleRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        listBleAdapter = new BleAdapter();
        listBleAdapter.setListener(new BleAdapter.Listener() {
            @Override
            public void onItemClick(int pos) {
                displayConnetingProgress();
                Toast.makeText(MainActivity.this, "Connecting to " + listBleAdapter.getBleDeviceOrAddressForIndex(pos), Toast.LENGTH_SHORT).show();

                bleUartHelper.connect(listBleAdapter.getBleAddressForIndex(pos));

                bleUartHelper.setCurrentConnectedDevice(listBleAdapter.getBleDeviceForIndex(pos));
            }
        });
        bleRecyclerView.setAdapter(listBleAdapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            askPermissionIfNeeded();
        }

        mBluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();


        startScan(true);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startService(new Intent(this, SmsReceiver.class));
                    startService(new Intent(this, NumberReceiver.class));
                    // startService(new Intent(this, DataProviderService.class));
                }
                return;
            }
        }
    }


    /** Records step data by requesting a subscription to background step data. */
    public void subscribe() {
        // To create a subscription, invoke the Recording API. As soon as the subscription is
        // active, fitness data will start recording.
        Fitness.getRecordingClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .addOnCompleteListener(
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.i(TAG, "Successfully subscribed!");
                                } else {
                                    Log.w(TAG, "There was a problem subscribing.", task.getException());
                                }
                            }
                        });
    }

    /**
     * Is Notification Service Enabled.
     * Verifies if the notification listener service is enabled.
     * Got it from: https://github.com/kpbird/NotificationListenerService-Example/blob/master/NLSExample/src/main/java/com/kpbird/nlsexample/NLService.java
     * @return True if eanbled, false otherwise.
     */
    private boolean isNotificationServiceEnabled(){
        String pkgName = getPackageName();
        final String flat = Settings.Secure.getString(getContentResolver(),
                ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (int i = 0; i < names.length; i++) {
                final ComponentName cn = ComponentName.unflattenFromString(names[i]);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private android.app.AlertDialog buildNotificationServiceAlertDialog(){
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.notification_listener_service);
        alertDialogBuilder.setMessage(R.string.notification_listener_service_explanation);
        alertDialogBuilder.setPositiveButton(R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS));
                    }
                });
        alertDialogBuilder.setNegativeButton(R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // If you choose to not enable the notification listener
                        // the app. will not work as expected
                    }
                });
        return(alertDialogBuilder.create());
    }
    

    private void goToSettingsActivity(String title) {
        Intent intent = new Intent(MainActivity.this, SettingOLEDActivity.class);
        intent.putExtra(SettingOLEDActivity.TITLE_DEVICE_PARAM, title);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the main; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    private void initializeLogging() {
        // Wraps Android's native log framework.
        LogWrapper logWrapper = new LogWrapper();
        // Filter strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);
        // On screen logging via a customized TextView.

        Log.i(TAG, "Ready");
    }

    /*
     * Ask permission to user if needed
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void askPermissionIfNeeded() {
        // Check if api level 23 or above
        // Android M Permission check
        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("This app needs location access");
            builder.setMessage("Please grant location to let app detect SmartEye device.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                }
            });
            builder.show();
        }
    }

    /*
     * Begin a scan for new servers that advertise our
     * matching service.
     */
    private void startScan(final boolean enable) {
        if(mBluetoothAdapter != null) {

            if (enable) {
                // Stops scanning after a pre-defined scan period.
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mScanning = false;
                        mBluetoothAdapter.getBluetoothLeScanner().stopScan(mScanCallback);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, SCAN_PERIOD);

                mScanning = true;
                mBluetoothAdapter.getBluetoothLeScanner().startScan(mScanCallback);
                swipeRefreshLayout.setRefreshing(true);
            } else {
                mScanning = false;
                mBluetoothAdapter.getBluetoothLeScanner().stopScan(mScanCallback);
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    private void refreshBleScan() {
        stopBleScan();
        listBleAdapter.clearDeviceList();
        startScan(true);
    }

    private void stopBleScan() {
        if(mBluetoothAdapter != null) {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(mScanCallback);
            mScanning = false;
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    /*
     * Callback handles results from new devices that appear
     * during a scan. Batch results appear when scan delay
     * filters are enabled.
     */
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            Log.d(TAG, "onScanResult");
            processResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            Log.d(TAG, "onBatchScanResults: "+results.size()+" results");

            for (ScanResult result : results) {
                processResult(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.w(TAG, "LE Scan Failed: "+errorCode);
        }



        private void processResult(final ScanResult result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    BluetoothDevice device = result.getDevice();
                    Log.i(TAG, "New LE Device: " + device.getName() + " @ " + result.getRssi());
                    if(device.getName() != null) {
                        //Add it to the collection
                        BleAdapter adapter = (BleAdapter) bleRecyclerView.getAdapter();
                        adapter.addDevice(device);
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }


    };

    private void InitializePullToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshBleScan();
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }


    // BLE PART
    private void uartServiceInit() {
        bleUartHelper.startUartService(this);
        bleUartHelper.addEventHandler(this);
    }

    // Receive Message
    @Override
    public void onMessageReceived(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG,"message -> "+ message);
               /* switch (message) {
                    case "calendar" :
                        bleUartHelper.sendMessage(getCalendarEvent(getBaseContext()));
                        break;
                }*/
            }
        });
    }

    @Override
    public void onDeviceConnected() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                Toast.makeText(MainActivity.this, "["+currentDateTimeString+"] Device connected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDeviceReady() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                Toast.makeText(MainActivity.this, "["+currentDateTimeString+"] Device ready", Toast.LENGTH_SHORT).show();
                //goToSettingsActivity(bleUartHelper.getCurrentConnectedDevice().getName());
                hideConnetingProgress();
            }
        });
        startService(new Intent(this, DataProviderService.class));
    }

    @Override
    public void onDeviceDisconnected() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                Toast.makeText(MainActivity.this, "["+currentDateTimeString+"] Disconnected", Toast.LENGTH_SHORT).show();
            }
        });
        stopService(new Intent(this, DataProviderService.class));
    }

    @Override
    public void onBluetoothInitializationFailed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Bluetooth init failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDeviceConnectionFailed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Device doesn't support UART. Disconnecting", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setSvg(int color, AnimatedSvgView.OnStateChangeListener listener) {
        final SVGLogoLoader svg = new SVGLogoLoader();
        svgView.setGlyphStrings(svg.getPath());
        svgView.setFillColor(color);
        svgView.setViewportSize(svg.getWidth(), svg.getHeight());
        svgView.setTraceResidueColor(0x32000000);
        svgView.setTraceColor(color);
        svgView.setOnStateChangeListener(listener);
        svgView.start();
    }

    private void displayConnetingProgress() {
        connectingMessageLayout.setVisibility(View.VISIBLE);
        setSvg(Color.WHITE, new AnimatedSvgView.OnStateChangeListener() {
            @Override
            public void onStateChange(int state) {
                if (state == AnimatedSvgView.STATE_FINISHED)
                    SVGfailure();
            }
        });
    }

    private void hideConnetingProgress() {
        setSvg(0xFF009688, new AnimatedSvgView.OnStateChangeListener() {
            @Override
            public void onStateChange(int state) {
                if (state == AnimatedSvgView.STATE_FINISHED)
                    SVGsuccess();
            }
        }); // SVGsuccess
    }


    private void SVGfailure() {
        if (cmp == timeout) {
            connectingMessageTextView.setText("Error during connection");
            setSvg(0xFFBA2121, new AnimatedSvgView.OnStateChangeListener() {
                @Override
                public void onStateChange(int state) {
                    if (state == AnimatedSvgView.STATE_FINISHED) {
                        Log.i("QSDF", state + "");
                        connectingMessageLayout.setVisibility(View.GONE);
                        cmp = 0;
                        connectingMessageTextView.setText(getString(R.string.connecting_to_device));
                    }
                }
            });
        }
        Log.e("CMPT", "CMPT = "+ cmp);
        cmp++;
        svgView.reset();
        svgView.start();
    }

    private void SVGsuccess() {
        Log.i("TIMEOUT", "ERROR CONNECTION TIMEOUT");
        connectingMessageTextView.setText("Connection success");

        connectingMessageLayout.animate().alpha(0f).setDuration(2000).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                Log.i("QSDF", "azertyuio");
                connectingMessageLayout.setVisibility(View.GONE);
                goToSettingsActivity(bleUartHelper.getCurrentConnectedDevice().getName());
//                            goToSettingsActivity("qwerty");
            }
        });

        /*setSvg(0xFFBA2121, new AnimatedSvgView.OnStateChangeListener() {
            @Override
            public void onStateChange(int state) {
                if (state == AnimatedSvgView.STATE_FINISHED) {
                    Log.i("QSDF", state +"");
                    connectingMessageLayout.animate().alpha(0f).setDuration(2000).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            Log.i("QSDF", "azertyuio");
                            connectingMessageLayout.setVisibility(View.GONE);
                            goToSettingsActivity(bleUartHelper.getCurrentConnectedDevice().getName());
//                            goToSettingsActivity("qwerty");
                        }
                    });
                }
            }
        });*/
    }

}
