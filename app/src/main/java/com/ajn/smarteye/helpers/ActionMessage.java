package com.ajn.smarteye.helpers;

import com.google.gson.Gson;

public class ActionMessage {
    private final String messageType = "action";
    private String target = "";
    private long value;

    public ActionMessage(String target, long value) {
        this.target = target;
        this.value = value;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getTarget() {
        return target;
    }

    public long getValue() {
        return value;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
