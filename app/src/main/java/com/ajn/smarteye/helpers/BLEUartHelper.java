package com.ajn.smarteye.helpers;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.ajn.smarteye.MainActivity;
import com.ajn.smarteye.services.UartService;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.ajn.smarteye.services.DeviceService.getCalendarEvent;

public class BLEUartHelper {
    private static BLEUartHelper instance = new BLEUartHelper();

    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_READY = 10;
    public static final String TAG = "AJN_BLE_UART_HELPER";
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;

    private int mState = UART_PROFILE_DISCONNECTED;
    private UartService uartService = null;
    private BluetoothDevice mDevice = null;

    private List<IBLEUartHelperEventHandler> eventHandlers;

    private BLEUartHelper() {
        // Block Default Constructor (Singleton)
        this.eventHandlers = new ArrayList<>();
    }

    public static BLEUartHelper getInstance() {
        return instance;
    }

    public void connect(String bleDeviceAddress) {
        uartService.connect(bleDeviceAddress);
    }

    public void disconnect() {
        uartService.disconnect();
        uartService.close();
    }

    public void addEventHandler(IBLEUartHelperEventHandler eventHandler) {
        this.eventHandlers.add(eventHandler);
    }

    public void sendMessage(String message) {
        if(mState == UART_PROFILE_READY) {
            byte[] bytes = message.getBytes();
            uartService.writeRXCharacteristic(bytes);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setCurrentConnectedDevice(BluetoothDevice device) {
        mDevice = device;
    }

    public BluetoothDevice getCurrentConnectedDevice() {
        return mDevice;
    }

    public void startUartService(Activity context) {
        Intent bindIntent = new Intent(context, UartService.class);
        
        context.stopService(bindIntent);

        context.startService(bindIntent);
        context.bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(context).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
    }

    //UART service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            uartService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + uartService);
            if (!uartService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                for (IBLEUartHelperEventHandler handler: eventHandlers) {
                    handler.onBluetoothInitializationFailed();
                }
            }

        }

        public void onServiceDisconnected(ComponentName classname) {
            uartService = null;
        }
    };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }

    private final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            final Intent mIntent = intent;
            //*********************//
            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                Log.d(TAG, "UART_CONNECT_MSG");
                mState = UART_PROFILE_CONNECTED;
                for (IBLEUartHelperEventHandler handler: eventHandlers) {
                    handler.onDeviceConnected();
                }
            }

            //*********************//
            if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                Log.d(TAG, "UART_DISCONNECT_MSG");

                mState = UART_PROFILE_DISCONNECTED;
                uartService.disconnect();
                uartService.close();

                for (IBLEUartHelperEventHandler handler: eventHandlers) {
                    handler.onDeviceDisconnected();
                }
            }


            //*********************//
            if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mState = UART_PROFILE_READY;
                uartService.enableTXNotification();
                for (IBLEUartHelperEventHandler handler: eventHandlers) {
                    handler.onDeviceReady();
                    BLEUartHelper bleUartHelper = BLEUartHelper.getInstance();
                    Date currentTime = Calendar.getInstance().getTime();
                    bleUartHelper.sendMessage(new ActionMessage("time",currentTime.getTime()/1000).toString());
                }
            }
            //*********************//
            if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {

                final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
                try {
                    String text = new String(txValue, "UTF-8");

                    for (IBLEUartHelperEventHandler handler: eventHandlers) {
                        handler.onMessageReceived(text);
                    }

                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
            //*********************//
            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)){

                uartService.disconnect();
                for (IBLEUartHelperEventHandler handler: eventHandlers) {
                    handler.onDeviceConnectionFailed();
                    handler.onDeviceDisconnected();
                }
            }


        }
    };

}
