package com.ajn.smarteye.helpers;

public interface IBLEUartHelperEventHandler {
    void onMessageReceived(String message);
    void onDeviceConnected();
    void onDeviceReady();
    void onDeviceDisconnected();
    void onBluetoothInitializationFailed();
    void onDeviceConnectionFailed();
}
