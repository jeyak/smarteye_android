package com.ajn.smarteye.helpers;

public class ViewOrderMessage {
    private String messageType = "";
    private String target = "";
    private ViewOrderObject value;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public ViewOrderObject getValue() {
        return this.value;
    }

    public void setValue(ViewOrderObject value) {
        this.value = value;
    }
}
