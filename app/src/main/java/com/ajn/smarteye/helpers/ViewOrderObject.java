package com.ajn.smarteye.helpers;

public class ViewOrderObject {
    private int clock = 1;
    private int calendar = 2;
    private int weather = 3;
    private int podometer = 4;

    public int getClock() {
        return clock;
    }

    public void setClock(int clock) {
        this.clock = clock;
    }

    public int getCalendar() {
        return calendar;
    }

    public void setCalendar(int calendar) {
        this.calendar = calendar;
    }

    public int getWeather() {
        return weather;
    }

    public void setWeather(int weather) {
        this.weather = weather;
    }

    public int getPodometer() {
        return podometer;
    }

    public void setPodometer(int podometer) {
        this.podometer = podometer;
    }
}
