package com.ajn.smarteye.helpers;

public class RequestMessage {
    private String messageType = "request";
    private String target = "";

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
