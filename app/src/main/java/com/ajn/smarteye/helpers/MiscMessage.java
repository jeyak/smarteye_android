package com.ajn.smarteye.helpers;

public class MiscMessage {
    private final String messageType = "misc";
    private String target = "";
    private String value = "";

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public MiscMessage(String target, String value) {
        this.target = target;
        this.value = value;
    }
}
