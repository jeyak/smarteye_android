package com.ajn.smarteye.helpers;

import com.ajn.smarteye.weather_api.models.Weather;

public class WeatherMessage {
    private String messageType = "";
    private String target = "";

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Weather getValue() {
        return value;
    }

    public void setValue(Weather value) {
        this.value = value;
    }

    private Weather value;
}
