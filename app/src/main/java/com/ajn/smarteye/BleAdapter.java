package com.ajn.smarteye;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BleAdapter extends RecyclerView.Adapter<BleAdapter.BleViewHolder> {

    private SparseArray<BluetoothDevice> mDevices = new SparseArray<>();

    private Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public BleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View bleItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ble, parent, false);
        return new BleViewHolder(bleItem);
    }

    @Override
    public void onBindViewHolder(@NonNull BleViewHolder holder, final int position) {

        holder.bleTitle.setText(getBleDeviceOrAddressForIndex(position));
        //holder.bleTitle.setText("azertyuiop");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if (listener != null) listener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    public void clearDeviceList() {
        mDevices.clear();
    }

    public class BleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_ble)
        TextView bleTitle;

        public BleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface Listener {
        void onItemClick(int pos);
    }

    public void addDevice(BluetoothDevice device) {
        mDevices.put(device.hashCode(), device);
    }

    public BluetoothDevice getBleDeviceForIndex(int index) {
        return mDevices.get(mDevices.keyAt(index));
    }

    public String getBleDeviceOrAddressForIndex(int index) {
        BluetoothDevice bleDevice = getBleDeviceForIndex(index);
        return bleDevice.getName() != null ? bleDevice.getName() : bleDevice.getAddress();
    }

    public String getBleAddressForIndex(int index) {
        BluetoothDevice bleDevice = getBleDeviceForIndex(index);
        return bleDevice.getAddress();
    }
}
