package com.ajn.smarteye.dragNdropHelper;

import com.ajn.smarteye.settings.SEWidget;

import java.util.List;

public interface IOnCustomerListChangedListener {
    void onNoteListChanged(List<SEWidget> seWidgetList);
}
