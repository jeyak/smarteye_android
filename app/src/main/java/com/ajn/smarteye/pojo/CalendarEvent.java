package com.ajn.smarteye.pojo;

import android.provider.CalendarContract;

/**
 * Created by apple on 13/06/2018.
 */

public class CalendarEvent {

    private String title;
    private String eventLocation;
    private String start;
    private String end;

    public CalendarEvent(String title, String eventLocation, String start, String end) {
        this.title = title;
        this.eventLocation = eventLocation;
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "CalendarNotification{" +
                "title='" + title + '\'' +
                ", eventLocation='" + eventLocation + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
