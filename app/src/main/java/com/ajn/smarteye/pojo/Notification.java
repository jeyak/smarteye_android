package com.ajn.smarteye.pojo;

import com.google.gson.Gson;

/**
 * Created by apple on 24/05/2018.
 */

public class Notification  {
    private String messageType;
    private String target;
    private String title;
    private String value;

    public Notification(String messageType, String target,String title, String value) {
        this.messageType = messageType;
        this.target = target;
        this.title = title;
        this.value = value;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}