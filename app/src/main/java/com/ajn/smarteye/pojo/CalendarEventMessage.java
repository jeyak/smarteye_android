package com.ajn.smarteye.pojo;

import com.google.gson.Gson;

/**
 * Created by apple on 13/06/2018.
 */

public class CalendarEventMessage {
    private String messageType; // calendar
    private String target; // calendar_event
    private CalendarEvent[] value;

    public CalendarEventMessage(String messageType, String target, CalendarEvent[] value) {
        this.messageType = messageType;
        this.target = target;
        this.value = value;
    }


    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
